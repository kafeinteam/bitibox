package com.agca.bitibox.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.agca.bitibox.funcs.Constants;

@WebFilter(filterName = "AuthFilter", urlPatterns = { "*.xhtml" })
public class PageFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		try {
			HttpServletRequest reqt = (HttpServletRequest) request;
			HttpServletResponse resp = (HttpServletResponse) response;
			HttpSession ses = reqt.getSession(false);

			String reqURI = reqt.getRequestURI();

			if (reqURI.contains(Constants.REGISTER_PAGE) 
					|| reqURI.contains(Constants.LOGIN_PAGE)
					|| reqURI.contains(Constants.HELP_PAGE)
					|| reqURI.contains(Constants.QUERY_PAGE)
					|| (ses != null && ses.getAttribute(Constants.EMAIL) != null)
					|| reqURI.contains("javax.faces.resource")) {
				chain.doFilter(request, response); // filtreleme yok
			} else {
				resp.sendRedirect(reqt.getContextPath() + "/login.xhtml");
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public static String getFullURL(HttpServletRequest request) {
		StringBuffer requestURL = request.getRequestURL();
		String queryString = request.getQueryString();

		if (queryString == null) {
			return requestURL.toString();
		} else {
			return requestURL.append('?').append(queryString).toString();
		}
	}

	@Override
	public void destroy() {

	}
}
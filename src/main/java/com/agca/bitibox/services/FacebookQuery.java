package com.agca.bitibox.services;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.agca.bitibox.funcs.Constants;
import com.agca.bitibox.funcs.Functions;
import com.restfb.json.JsonObject;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;

@WebServlet("/Query")
public class FacebookQuery extends HttpServlet {
	private static final long serialVersionUID = 1L;

	final static Logger logger = Logger.getLogger(FacebookQuery.class);

	public FacebookQuery() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		JsonObject json = new JsonObject();

		// �r�n�m�z product id sini gonderdi
		String productId;
		try {
			productId = request.getParameter(Constants.PRODUCT_ID).trim();
		} catch (Exception e) {
			json.put(Constants.SUCCESS, false);
			json.put(Constants.ERROR_MSG, "product id bos olamaz!");

			out.print(json);
			out.flush();

			return;
		}

		// product i�in kay�tl� facebook id al�nacak
		String facebookId;
		try {
			facebookId = Functions.getMyFacebookId(productId);

			if (facebookId.trim().isEmpty()) {
				json.put(Constants.SUCCESS, false);
				json.put(Constants.ERROR_MSG, "facebook id bulunamadi!");

				out.print(json);
				out.flush();
				return;
			}

		} catch (Exception e) {
			logger.debug("facebook id veritabanindan alinamadi!");
			return;
		}

		// bu facebookId in be�eni say�s� �ekilecek
		String likeCount = getLikeCount(facebookId);

		json.put(Constants.SUCCESS, true);
		json.put(Constants.FACEBOOK_ID, facebookId);
		json.put(Constants.LIKE_COUNT, likeCount);

		out.print(json);
		out.flush();

	}

	//FacebookClient "restFb" k�t�phanesinden al�nm��t�r, test ediliyor.
	private String getLikeCount(String facebookId) {
		
		logger.debug(facebookId);
		
		@SuppressWarnings("deprecation")
		FacebookClient facebookClient = new DefaultFacebookClient(Constants.MY_ACCESS_TOKEN);
		JsonObject json = facebookClient.fetchObject(facebookId, JsonObject.class, Parameter.with("fields", "likes"));

		String likes = json.getString(Constants.LIKES);

		return likes;
	}

}

package com.agca.bitibox.beans;

import java.io.Serializable;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import org.apache.log4j.Logger;

import com.agca.bitibox.funcs.Functions;

@ManagedBean(name = "Product")
@ApplicationScoped
public class Product implements Serializable {

	private static final long serialVersionUID = 1L;

	final static Logger logger = Logger.getLogger(Product.class);

	String productId;
	String facebookId;
	String email;

	public Product() {
		productId = Functions.getMyProductId();
		facebookId = Functions.getMyFacebookId();
	}

	public String getProductId() {

		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
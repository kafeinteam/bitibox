package com.agca.bitibox.funcs;

public class Constants {

	public static final String USERS = "users";
	public static final String EMAIL = "email";
	public static final String PASSWORD = "password";
	
	public static final String PRODUCTS = "products";
	public static final String PRODUCT_ID = "product_id";
	public static final String FACEBOOK_ID = "facebook_id";
	
	public static final String LIKE_COUNT = "like_count";
	
	
	public static final int LOGIN_EXPIRE_TIME = 30 * 60; //ms
	
	public static final String LOGIN_PAGE = "login";
	public static final String REGISTER_PAGE = "register";
	public static final String PRODUCT_PAGE = "product";
	public static final String HELP_PAGE = "help";
	public static final String QUERY_PAGE = "Query";
	public static final String SUCCESS = "success";
	public static final String ERROR_MSG = "error_msg";
	
	
	public static final String ACCESS_TOKEN = "access_token"; //for facebook
	public static final String MY_ACCESS_TOKEN = "674012852699233|AeqaQw0SJzAcQsB5-iOuC_PF8Vs";
	public static final String LIKES = "likes";
}

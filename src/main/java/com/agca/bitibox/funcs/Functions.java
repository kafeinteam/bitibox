package com.agca.bitibox.funcs;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "Functions")
@ApplicationScoped
public class Functions implements Serializable {

	private static final long serialVersionUID = 1L;

	final static Logger logger = Logger.getLogger(Functions.class);

	private static Connection connection = null;
	private final static String DB_LINK = "jdbc:mysql://localhost/bitibox_db";
	private final static String DB_USER = "root";
	private final static String DB_PASS = "";

	public static void connectionOpen() {

		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			logger.error("class not found!", e);
			return;
		}

		try {
			connection = DriverManager.getConnection(DB_LINK, DB_USER, DB_PASS);

		} catch (SQLException e) {
			logger.error("connection error!", e);
			return;
		}

		if (connection != null) {
			logger.info("database connected.");
		} else {
			logger.error("Failed to make connection!");
		}
	}

	
	public static String login(String email, String password) {

		RequestContext context = RequestContext.getCurrentInstance();
		FacesMessage message = null;
		boolean success = false;
		String returnPage = Constants.LOGIN_PAGE;

		if (!email.trim().isEmpty() && !password.trim().isEmpty()) {
			if (Functions.loginDB(email, password)) {
				
				Session.setEmail(email);
				
				success = true;
				message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Ho�geldiniz", email);

				returnPage = Constants.PRODUCT_PAGE;

			} else {
				success = false;
				message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Giri� Ba�ar�s�z!",
						"Kullan�c� bilgileri hatal� girildi");
			}
		} else {
			success = false;
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Giri� ba�ar�s�z!", "Alanlar bo� b�rak�lamaz");
		}

		FacesContext.getCurrentInstance().addMessage(null, message);
		context.addCallbackParam("success", success);
		return returnPage;
	}

	public static boolean loginDB(String email, String password) {

		connectionOpen();

		PreparedStatement statement = null;
		String query = "SELECT * FROM users WHERE " + Constants.EMAIL + " = ? AND " + Constants.PASSWORD + " = ?";
		try {
			statement = connection.prepareStatement(query);
			statement.setString(1, email);
			statement.setString(2, password);

			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				logger.debug("Giri� ba�ar�l�");
				return true;
			}

			statement.close();
			connection.close();

		} catch (SQLException e) {
			logger.error("Giri� ba�ar�s�z!", e);
		}

		return false;
	}
	
	public static String logout() {
		Session.logout();
		return Constants.LOGIN_PAGE;
	}
	
	public static void changePassword(String newPassword) {
		
		RequestContext context = RequestContext.getCurrentInstance();
		FacesMessage message = null;
		boolean success = false;
		
		connectionOpen();

		PreparedStatement statement = null;
		String query = "UPDATE " + Constants.USERS + " SET " + Constants.PASSWORD + " = ? " + "WHERE "
				+ Constants.EMAIL + " = ?";
		try {
			statement = connection.prepareStatement(query);

			statement.setString(1, newPassword);
			statement.setString(2, Session.getEmail());
			statement.executeUpdate();

			logger.debug("�ifre de�i�tirildi");
			statement.close();
			connection.close();
			
			success = true;
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "",
					"�ifre De�i�tirildi");
			
		} catch (SQLException e) {
			logger.error("�ifre de�i�tirilemedi!", e);
			success = false;
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "",
					"�ifre De�i�tirilemedi!");
		}
		
		FacesContext.getCurrentInstance().addMessage(null, message);
		context.addCallbackParam("success", success);
	}

	
	public static String register(String email, String password) {
		connectionOpen();

		RequestContext context = RequestContext.getCurrentInstance();
		FacesMessage message = null;
		boolean success = false;
		String returnPage = Constants.REGISTER_PAGE;

		PreparedStatement statement = null;
		String query = "INSERT INTO users(" + Constants.EMAIL + ", " + Constants.PASSWORD
				+ ") VALUES(?, ?, ?)";
		try {
			statement = connection.prepareStatement(query);
			statement.setString(1, email);
			statement.setString(2, password);
			statement.executeUpdate();

			logger.debug("Kullan�c� kaydedildi");
			statement.close();
			connection.close();

			success = true;
			message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Kay�t ba�ar�l�, �imdi giri� yap�n�z...", email);

			returnPage = Constants.LOGIN_PAGE;
		} catch (SQLException e) {
			logger.error("Kay�t hatas�!", e);

			success = false;
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Kay�t Ba�ar�s�z!", "Bir hata meydana geldi");
		}

		FacesContext.getCurrentInstance().addMessage(null, message);
		context.addCallbackParam("success", success);
		return returnPage;
	}

	
	public void addProduct(String productId, String facebookId) {
		RequestContext context = RequestContext.getCurrentInstance();
		FacesMessage message = null;
		boolean success = false;

		if (!productId.trim().equals("") & !facebookId.trim().equals("")) {
			if (addProductDB(productId, facebookId, Session.getEmail())) {

				success = true;
				message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Kaydedildi ", productId);
			} else {
				success = false;
				message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Kaydetme Hatas�!", "");
			}
		} else {
			success = false;
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Hata", "Alanlar bo� b�rak�lamaz!");
		}

		FacesContext.getCurrentInstance().addMessage(null, message);
		context.addCallbackParam(Constants.SUCCESS, success);
	}

	public static boolean addProductDB(String productId, String facebookId, String email) {
		
		connectionOpen();

		PreparedStatement statement = null;
		String query = "INSERT INTO " + Constants.PRODUCTS + "(" + Constants.PRODUCT_ID + ", " 
				+ Constants.FACEBOOK_ID + ", " + Constants.EMAIL + ") VALUES(?, ?, ?)";
		try {
			statement = connection.prepareStatement(query);

			statement.setString(1, productId);
			statement.setString(2, facebookId);
			statement.setString(3, email);
			statement.executeUpdate();

			logger.debug("�r�n eklendi");
			statement.close();
			connection.close();

			return true;
		} catch (SQLException e) {
			logger.error("�r�n eklenemedi!", e);
		}

		return false;
	}

	
	public static boolean isAddedProduct() {

		connectionOpen();

		PreparedStatement statement = null;
		String query = "SELECT * FROM " + Constants.PRODUCTS + " WHERE " + Constants.EMAIL + " = ?";
		try {
			statement = connection.prepareStatement(query);

			statement.setString(1, Session.getEmail());
			ResultSet resultSet = statement.executeQuery();

			if (resultSet.next()) {
				return true;
			}
			statement.close();
			connection.close();

		} catch (SQLException e) {
			logger.error("get products error!", e);
		}

		return false;

	}
	
	
	public static boolean changeProductId(ValueChangeEvent event) {
		
		String newProductId = event.getNewValue().toString();
		
		connectionOpen();

		PreparedStatement statement = null;
		String query = "UPDATE " + Constants.PRODUCTS + " SET " + Constants.PRODUCT_ID + " = ? " + "WHERE "
				+ Constants.EMAIL + " = ?";
		try {
			statement = connection.prepareStatement(query);

			statement.setString(1, newProductId);
			statement.setString(2, Session.getEmail());
			statement.executeUpdate();

			logger.debug("�r�n id de�i�tirildi");
			statement.close();
			connection.close();

			return true;
		} catch (SQLException e) {
			logger.error("�r�n id de�i�tirilemedi!", e);
		}

		return false;
	}

	public static boolean changeFacebookId(ValueChangeEvent event) {
		
		String newFacebookId = event.getNewValue().toString();
		
		connectionOpen();

		PreparedStatement statement = null;
		String query = "UPDATE " + Constants.PRODUCTS + " SET " + Constants.FACEBOOK_ID + " = ? " + "WHERE "
				+ Constants.EMAIL + " = ?";
		try {
			statement = connection.prepareStatement(query);

			statement.setString(1, newFacebookId);
			statement.setString(2, Session.getEmail());
			statement.executeUpdate();

			logger.debug("facebook id de�i�tirildi");
			statement.close();
			connection.close();

			return true;
		} catch (SQLException e) {
			logger.error("facebook id de�i�tirilemedi!", e);
		}

		return false;
	}

	
	public static String getMyProductId() {

		connectionOpen();

		String myProductId = "";

		PreparedStatement statement = null;
		String query = "SELECT * FROM " + Constants.PRODUCTS + " WHERE " + Constants.EMAIL + " = ?";
		try {
			statement = connection.prepareStatement(query);

			statement.setString(1, Session.getEmail());
			ResultSet resultSet = statement.executeQuery();

			if (resultSet.next()) {
				myProductId = resultSet.getString(Constants.PRODUCT_ID);
			}
			statement.close();
			connection.close();

		} catch (SQLException e) {
			logger.error("get products error!", e);
		}

		return myProductId;
	}
	
	public static String getMyFacebookId() {

		connectionOpen();

		String myFacebookId = "";

		PreparedStatement statement = null;
		String query = "SELECT * FROM " + Constants.PRODUCTS + " WHERE " + Constants.EMAIL + " = ?";
		try {
			statement = connection.prepareStatement(query);

			statement.setString(1, Session.getEmail());
			ResultSet resultSet = statement.executeQuery();

			if (resultSet.next()) {
				myFacebookId = resultSet.getString(Constants.FACEBOOK_ID);
			}
			statement.close();
			connection.close();

		} catch (SQLException e) {
			logger.error("get facebook id error!", e);
		}

		return myFacebookId;
	}
	
	public static String getMyFacebookId(String productId) {

		connectionOpen();

		String myFacebookId = "";

		PreparedStatement statement = null;
		String query = "SELECT * FROM " + Constants.PRODUCTS + " WHERE " + Constants.PRODUCT_ID + " = ?";
		try {
			statement = connection.prepareStatement(query);

			statement.setString(1, productId);
			ResultSet resultSet = statement.executeQuery();

			if (resultSet.next()) {
				myFacebookId = resultSet.getString(Constants.FACEBOOK_ID);
			}
			statement.close();
			connection.close();

		} catch (SQLException e) {
			logger.error("get facebook id error!", e);
		}

		return myFacebookId;
	}
	
}
